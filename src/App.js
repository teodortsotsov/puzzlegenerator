import Client from './components/Client';

function App() {
    return (
        <div className="App">
            <div>
                <Client />
            </div>
        </div>
    );
}

export default App;
