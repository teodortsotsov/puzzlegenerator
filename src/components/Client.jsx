import React, { useState } from "react";
import InputForm from "./InputForm";
import Utility from "./common/utility";

function Client() {
    const [wordsArray, setWordsArray] = useState([]);
    const handleFormSubmit = (words) => {
        let objMap = Utility.createCharacterMap(words);
        let markedCharactersArray = Object.keys(objMap);
        console.log(markedCharactersArray);
        Utility.shuffleArray(markedCharactersArray);
        console.log(markedCharactersArray);
        Utility.shuffleArray(markedCharactersArray);
        console.log(markedCharactersArray);
        setWordsArray(words);
    }
    return (
        <div className="client">
            <header className="client-header">
                Puzzle Generator
            </header>
            <div>
                {wordsArray.length === 0 ? <InputForm onSubmit={handleFormSubmit} /> : null}
            </div>
        </div>
    );
}

export default Client;