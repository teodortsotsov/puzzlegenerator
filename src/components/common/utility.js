const Utility = {
    //Creates a character map with coordinates: wordIndex - the index of the word, charIndex - the index of the char in the word
    createCharacterMap: function (wordsArr) {
        let mapObject = {};
        let mapObjectResult = {};
        console.log('func', wordsArr);
        wordsArr.forEach((word, wordIndex) => {
            word.value.split('').forEach((char, charIndex) => {
                if (mapObject[char]) {
                    mapObject[char].coordinates.push({ wordIndex, charIndex });
                } else {
                    mapObject[char] = { coordinates: [{ wordIndex, charIndex }] };
                }
            });
        });

        for (const key in mapObject) {
            if (mapObject[key]?.coordinates.length > 1) {
                mapObjectResult[key] = mapObject[key];
            }
        }
        return mapObjectResult;
    },
    shuffleArray: function (array) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
};


export default Utility;