import React from "react";

const Field = ({ field, handleFieldChange }) => {
    return (
        <li key={field.id}>
            <label htmlFor={field.id}>{field.name}</label>
            <input
                type={field.type}
                id={field.name}
                name={`input${field.id}`}
                value={field.value}
                onChange={e => handleFieldChange(field.id, e.target.value)}
            />
        </li>
    );
};

export default Field;