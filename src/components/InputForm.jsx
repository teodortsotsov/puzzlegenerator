import React, { useState } from "react";
import Field from "./common/Field";

const Form = ({ onSubmit }) => {
    const [words, setWords] = useState([{ value: '', type: 'input' }]);
    const addWord = () => {
        words.push({ value: '', type: 'input' });
        console.log('words', words);
        setWords([...words]);
    };
    const removeWord = index => {
        words.splice(index, 1)
        setWords([...words]);
    };

    const onFieldChange = (index, value) => {
        console.log(index, value);
        words[index].value = value;
        setWords([...words]);
    }

    const handleFormSubmit = () => {
        onSubmit(words);
    }

    return (
        <form onSubmit={handleFormSubmit}>
            <h2>Add puzzle content words</h2>
            <div className="form-content">
                <ul>
                    {
                        words.map((el, index) => (
                            <div>
                                <Field field={{ id: index, value: el.value, type: 'input' }} handleFieldChange={onFieldChange} />
                                {index > 0 ? <button type="button" onClick={() => removeWord(index)}>Remove</button> : null}
                            </div>
                        ))
                    }
                </ul>
                <div className="button-group">
                    <button type="button" onClick={addWord}>Add new</button>
                </div>
            </div>
            <div className="button-group">
                <button type="submit">Generate Puzzle</button>
            </div>
        </form>
    );
};

export default Form;