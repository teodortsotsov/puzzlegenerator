# Puzzle Generator Solution Description
1. I intend to use a character map which can be randomized, it is helpful to know all the relations.
example:
{
  "L": {
    "coordinates": [
      {
        "wordIndex": 0,
        "charIndex": 2
      },
      {
        "wordIndex": 0,
        "charIndex": 3
      },
      {
        "wordIndex": 1,
        "charIndex": 3
      }
    ]
  },
  "O": {
    "coordinates": [
      {
        "wordIndex": 0,
        "charIndex": 4
      },
      {
        "wordIndex": 1,
        "charIndex": 1
      }
    ]
  }
}

2. I need a matrix object/two dimensinal array with dimensions: width = height = (sum of all word.length)
example:
resultArr = [[' ',' ',' ',' ','W',' ',' ' ........],
			 ['H','E','L','L','0',' ',' ' ........],
			 
			 ....................
			 ]
it can be drawn as table - tr/td or with divs - flex - flex-direction: row/column
3. resultArr should contain only empty values - ' '
4.First after having the keys array of the characterMapObject shuffled then I can loop the object by keys. I can pick two items for the coordinates array which have different wordIndex property which means two differen words, then I have to get a random of two numbers 0/1 - horizontal/vertical and then will calculate the offset and will start filling the visual matrix. 
5.During the filling of the matrix I can check if every character exists in the map and I can pick randomly another word which has different wordIndex then will calculate the offsets and if it doesn't exceed the borders I'll draw it. If it exceeds the borders then it will be skiped.
